package com.example.vishal.machine_test_task;

public class Item {

    private String status, file, pitch, lips, maleConfidence;

    public Item(String status, String file, String pitch, String lips, String maleConfidence) {

        this.status = status;
        this.file = file;
        this.pitch = pitch;
        this.lips = lips;
        this.maleConfidence = maleConfidence;

    }

    public String getStatus() {
        return status;
    }

    public String getFile() {
        return file;
    }

    public String getPitch() {
        return pitch;
    }

    public String getLips() {
        return lips;
    }

    public String getMaleConfidence() {
        return maleConfidence;
    }


}
