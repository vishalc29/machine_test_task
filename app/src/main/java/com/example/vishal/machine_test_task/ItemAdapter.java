package com.example.vishal.machine_test_task;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<Item> mExampleList;

    public ItemAdapter(Context context, ArrayList<Item> exampleList) {
        mContext = context;
        mExampleList = exampleList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_item_layout, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int i) {

        Item currentItem = mExampleList.get(i);

        String status = currentItem.getStatus();
        String file = currentItem.getFile();
        String pitch = currentItem.getPitch();
        String lips = currentItem.getLips();
        String maleConfidence = currentItem.getMaleConfidence();

        itemViewHolder.mTextViewSatus.setText(status);
        itemViewHolder.mTextViewFile.setText(file);
        itemViewHolder.mTextViewPitch.setText(pitch);
        itemViewHolder.mTextViewLips.setText(lips);
        itemViewHolder.mTextViewMaleConfidence.setText(maleConfidence);

    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewSatus;
        public TextView mTextViewFile;
        public TextView mTextViewPitch;
        public TextView mTextViewLips;
        public TextView mTextViewMaleConfidence;

        public ItemViewHolder(View itemView) {
            super(itemView);

            mTextViewSatus=itemView.findViewById(R.id.tvStatus);
            mTextViewFile=itemView.findViewById(R.id.tvFile);
            mTextViewPitch=itemView.findViewById(R.id.tvPitch);
            mTextViewLips=itemView.findViewById(R.id.tvLips);
            mTextViewMaleConfidence=itemView.findViewById(R.id.tvMaleConfidence);

        }
    }
}
