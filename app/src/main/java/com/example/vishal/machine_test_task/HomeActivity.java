package com.example.vishal.machine_test_task;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ItemAdapter mExampleAdapter;
    private ArrayList<Item> mExampleList;
    private RequestQueue mRequestQueue;
    private android.app.AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        alertDialog = new SpotsDialog.Builder().setContext(this).setMessage("please wait...").setCancelable(false).build();

        mRecyclerView = findViewById(R.id.recycler);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mExampleList = new ArrayList<>();

        mRequestQueue = Volley.newRequestQueue(this);

        parseData();

        alertDialog.show();

    }

    private void parseData() {


        String URL = "https://vishalc22.000webhostapp.com/sample.php";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                      JSONObject jsonObject = new JSONObject(response);

                      JSONArray jsonArray = jsonObject.getJSONArray("images");

                      for(int i=0;i<jsonArray.length();i++)
                      {
                          jsonObject = jsonArray.getJSONObject(i);

                          String status = jsonObject.getString("status");
                          String file = jsonObject.getString("file");


                          JSONArray jsonArray1 = jsonObject.getJSONArray("faces");


                          JSONObject jsonObject1 = jsonArray1.getJSONObject(0);

                          String pitch = jsonObject1.getString("pitch");

                          jsonObject1 = jsonObject1.getJSONObject("attributes");

                          String lips = jsonObject1.getString("lips");

                          jsonObject1 = jsonObject1.getJSONObject("gender");

                          String maleConfidence = jsonObject1.getString("maleConfidence");

                          mExampleList.add(new Item(status,file,pitch,lips,maleConfidence));

                      }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.w("ExceptionArray",e.toString());
                }


                mExampleAdapter = new ItemAdapter(HomeActivity.this, mExampleList);
                mRecyclerView.setAdapter(mExampleAdapter);
                alertDialog.dismiss();



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        mRequestQueue.add(stringRequest);

    }

    public void logout(View view) {

        Toast.makeText(this, "Succesufully Logout", Toast.LENGTH_SHORT).show();
        SharedPrefManager.getInstance(getApplicationContext()).logout();
        startActivity(new Intent(HomeActivity.this,LoginActivity.class));
        finish();
    }
}
