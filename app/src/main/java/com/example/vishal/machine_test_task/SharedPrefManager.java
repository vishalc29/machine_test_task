package com.example.vishal.machine_test_task;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {

    private static SharedPrefManager mInstance;
    private static Context mContext;
    private SharedPreferences sharedPreferences;

    private SharedPrefManager(Context context) {
        mContext = context;
        sharedPreferences = mContext.getSharedPreferences("machineTestData",Context.MODE_PRIVATE);
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }

        return mInstance;
    }

    public boolean saveData(String api_token, String name, String email){

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("api_token",api_token);
        editor.putString("name",name);
        editor.putString("email",email);
        editor.apply();


        return  true;
    }

    public boolean isLoggedin() {
        if (sharedPreferences.getString("email", null) != null) {

            return true;
        }

        return false;

    }

    public boolean logout () {

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.clear();

        editor.apply();

        return true;
    }

    public String getApiToken() {

        return sharedPreferences.getString("api_token","");
    }

    public String getEmail() {

        return sharedPreferences.getString("email","");
    }

    public String getName() {

        return sharedPreferences.getString("name","");
    }
}
