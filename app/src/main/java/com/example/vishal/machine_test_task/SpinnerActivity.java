package com.example.vishal.machine_test_task;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SpinnerActivity extends AppCompatActivity implements View.OnClickListener {


    String api_token;
    ArrayList<String> CountryName, StateName, CityName;
    ArrayAdapter<String> Cadapter, Sadapter, CityAdapter;
    Spinner spinner, spinner2, spinner3;
    Button btnHome;
    String countrySelected, stateSelected, citySelected, stateChangeUrl, cityChangeUrl;
    int countrySelectedId, stateSelectedId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        api_token = SharedPrefManager.getInstance(getApplicationContext()).getApiToken();

        spinner = findViewById(R.id.spinner);
        spinner2 = findViewById(R.id.spinner2);
        spinner3 = findViewById(R.id.spinner3);
        btnHome = findViewById(R.id.btnHome);

        CountryName = new ArrayList<>();
        CountryName.add("Countries Name");
        fetchDataCountry(CountryName);
        Cadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, CountryName);
        Cadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(Cadapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                countrySelected = spinner.getSelectedItem().toString();
                countrySelectedId = spinner.getSelectedItemPosition();


                stateChangeUrl = Constants.tempStateUrl + countrySelectedId + Constants.TEMP_URL + api_token;
                fetchDataState();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        StateName = new ArrayList<>();
        StateName.add("State Name");
        Sadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, StateName);
        Sadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner2.setAdapter(Sadapter);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                stateSelected = spinner2.getSelectedItem().toString();
                stateSelectedId = spinner2.getSelectedItemPosition();

                cityChangeUrl = Constants.tempCityUrl + stateSelectedId + "/0" + Constants.TEMP_URL + api_token;
                fetchDataCity();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        CityName = new ArrayList<>();
        CityName.add("City Name");
        CityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, CityName);
        CityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner3.setAdapter(CityAdapter);
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                citySelected = spinner2.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnHome.setOnClickListener(this);


    }


    private void fetchDataCity() {

        CityName.clear();
        CityName.add("City Name");


        StringRequest stringRequest = new StringRequest(Request.Method.GET, cityChangeUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("cities");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject j_obj = jsonArray.getJSONObject(i);

                        String name = j_obj.getString("name");

                        CityName.add(name);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void fetchDataState() {

        StateName.clear();
        StateName.add("State Name");


        StringRequest stringRequest = new StringRequest(Request.Method.GET, stateChangeUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("states");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject j_obj = jsonArray.getJSONObject(i);

                        String name = j_obj.getString("name");

                        StateName.add(name);


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.w("erorror", error);

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void fetchDataCountry(ArrayList<String> countryName) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.Country_URL + api_token, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("states");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject j_obj = jsonArray.getJSONObject(i);

                        String name = j_obj.getString("name");

                        CountryName.add(name);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {


        if (spinner.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Please! select yout country", Toast.LENGTH_SHORT).show();

        } else {
            if (spinner2.getSelectedItemPosition() == 0) {
                Toast.makeText(this, "Please! select yout state", Toast.LENGTH_SHORT).show();

            } else {
                if (spinner3.getSelectedItemPosition() == 0) {
                    Toast.makeText(this, "Please! select yout city", Toast.LENGTH_SHORT).show();

                } else {
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();

                }

            }
        }
        ;


    }


}

