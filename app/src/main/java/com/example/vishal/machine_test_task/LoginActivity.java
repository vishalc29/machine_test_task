package com.example.vishal.machine_test_task;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import dmax.dialog.SpotsDialog;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etLoginEmail, etLoginPassword;
    private Button btnLogin;
    private ProgressDialog progressDialog;
    String email, password;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkLoggedIn();

        etLoginEmail = findViewById(R.id.etEmail);
        etLoginPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);


        btnLogin.setOnClickListener(this);


    }

    private void checkLoggedIn() {

        if (SharedPrefManager.getInstance(getApplicationContext()).isLoggedin()) {

            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
            finish();

        }
    }


    @Override
    public void onClick(View v) {

        if (btnLogin == v) {

            if (etLoginEmail.equals(" ") && etLoginPassword.equals(" ")) {
                Toast.makeText(this, "Please enter valid detail", Toast.LENGTH_SHORT).show();
            } else {
                LoginUser();

            }


        }
    }

    private void LoginUser() {

        email = etLoginEmail.getText().toString();
        password = etLoginPassword.getText().toString();

        alertDialog = new SpotsDialog.Builder().setContext(this).setMessage("please wait...").setCancelable(false).build();

        alertDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    JSONObject jsonObject = new JSONObject(response).getJSONObject("user");
                    String api_token = jsonObject.getString("api_token");
                    String name = jsonObject.getString("name");
                    String email = jsonObject.getString("email");

                    SharedPrefManager.getInstance(getApplicationContext()).saveData(api_token, name, email);

                    alertDialog.dismiss();
                    startActivity(new Intent(getApplicationContext(), SpinnerActivity.class));
                    finish();


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                alertDialog.dismiss();
                Toast.makeText(LoginActivity.this, "Please enter valid detail", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected HashMap<String, String> getParams() throws AuthFailureError {

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("email", email);
                hashMap.put("password", password);

                return hashMap;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}

